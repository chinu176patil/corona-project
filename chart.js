// for adding charts
$(document).ready(function(){
    var url="https://data.covid19india.org/data.json"
    $.getJSON(url,function(data){
        var state=[ ]
        var statecode=[]
        var confirmed=[ ] 
        var active=[ ]
        var deaths=[ ]
        var recovered=[]
        $.each(data.statewise,function(id,obj){
            state.push(obj.state)
            statecode.push(obj.statecode)
            confirmed.push(obj.confirmed)
            active.push(obj.active)
            deaths.push(obj.deaths)
            recovered.push(obj.recovered)
        })
        console.log(data["statewise"].length)
        state.shift()
        statecode.shift()
        confirmed.shift()
        active.shift()
        deaths.shift()
        recovered.shift()
    
        var chart1=document.getElementById("mychart").getContext('2d')
        var chart=new Chart(chart1,{
            type:'line',
            data:{
                labels:statecode,
                datasets:[
                    {
                        label:"Confirmed Cases",
                        data:confirmed,
                        // hoverBorderColor : "#000",
                        borderColor: "#007bff", 
                        minBarLength: 5000

                    },
                    {
                        label:"Active Cases",
                        data:active,
                        borderColor:"#17a2b8",
                        minBarLength: 500
                    },
                    {
                        label:"Recovered Cases",
                        data:recovered,
                        // hoverBorderColor : "#000",
                        borderColor: "#28a745", 
                        minBarLength: 5000
                    },
                    {
                        label:"Deaths",
                        data:deaths,
                        // hoverBorderColor : "#000",
                        borderColor:"#dc3545", 
                        minBarLength: 5000
                        
                    }

                ]     
            },
            options:{
                title:{
                    display:true,
                    text:"Statewise Corona Cases",
                    fontColor:"#dc3545",
                    fontSize:12
                }
            }
        })
    
    })
})    




 
