$( window ).on("load",function(){    
    var url1="https://data.covid19india.org/data.json"
    $.getJSON(url1,function(data1){
        console.log(data1)
        console.log(data1["statewise"].length)
        var total_cases,total_recovered,total_active,total_death
    
        total_cases=data1.statewise[0].confirmed
        total_active=data1.statewise[0].active
        total_recovered=data1.statewise[0].recovered
        total_death=data1.statewise[0].deaths

        document.getElementById("active").innerHTML=total_active
        document.getElementById("confirmed").innerHTML=total_cases
        document.getElementById("recovered").innerHTML=total_recovered
        document.getElementById("deaths").innerHTML=total_death

        var tbval=document.getElementById('cases')
        for(var i=1; i<data1["statewise"].length;i++){
            var x=tbval.insertRow();
            console.log(data1["statewise"][i-1]["state"])     
                
            x.insertCell(0);   
            tbval.rows[i].cells[0].innerHTML=data1["statewise"][i]["state"]; 
            tbval.rows[i].cells[0].style.color="black";
            tbval.rows[i].cells[0].style.hover="grey";

            x.insertCell(1);   
            tbval.rows[i].cells[1].innerHTML=data1["statewise"][i]["confirmed"];   
            tbval.rows[i].cells[1].style.color="blue";

            x.insertCell(2);   
            tbval.rows[i].cells[2].innerHTML=data1["statewise"][i]["active"];
            tbval.rows[i].cells[2].style.color="skyblue";

            x.insertCell(3);   
            tbval.rows[i].cells[3].innerHTML=data1["statewise"][i]["recovered"] ;  
            tbval.rows[i].cells[3].style.color="green";

            x.insertCell(4);   
            tbval.rows[i].cells[4].innerHTML=data1["statewise"][i]["deaths"] ;  
            tbval.rows[i].cells[4].style.color="red";
        
        }


    })



    // var url="https://api.covid19india.org/state_district_wise.json"
    // $.getJSON(url,function(data2){
    //     console.log(data2)
    // })
})


   
